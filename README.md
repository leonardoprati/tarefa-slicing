# Slicing Service

##  Estrutura do repositório

### Diretório "worker":
Contém os arquivos necessários para executar o seu programa de corte de vídeo. Escreva sua solução no arquivo "worker.py"

### Diretório "api":
Contém os arquivos necessários para servir sua API. Escreva sua solução no arquivo "api.py"

### Arquivo "docker-compose.yml"
Define como o serviço será executado. Utilizamos o Docker para construir o ambiente para execução do serviço, e este arquivo, em conjunto com os arquivos Dockerfile dentro dos diretórios acima, definem como o ambiente funcionará. Não precisa alterar nada nestes.

## Para executar o serviço
``` sudo docker-compose up ```

## Para finalizar a execução
``` Ctrl + C ```
