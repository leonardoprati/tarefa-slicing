import redis
import json

class MQueue:
    def __init__(self, name, namespace='queue', host='redis', port=6379, db=0):
        self.db = redis.Redis(host=host, port=port, db=db)
        self.queue_id = f"{namespace}:{name}"

    def is_empty(self):
        return self.get_size() == 0

    def get_size(self):
        return self.db.llen(self.queue_id)

    def put(self, item):
        if not isinstance(item, str):
            item = json.dumps(item)
        self.db.rpush(self.queue_id, item)

    def get(self, block=True, timeout=None):
        if block:
            item = self.db.blpop(self.queue_id, timeout=timeout)
        else:
            item = self.db.lpop(self.queue_id)

        if item:
            item = item[1]
            item = json.loads(item)
        return item

    def get_nowait(self):
        return self.get(False)
