from gstorage import *
from message_queue import MQueue

import json
import threading
import subprocess
import time

fila = MQueue('mensagens')

class processThread ():

    def __init__(self,bucket_name,file_name,slice_time):
        threading.Thread.__init__(self)

        self.bucket_name = bucket_name
        self.file_name =  file_name
        self.slice_time = slice_time
        thread = threading.Thread(target=self.run, args=())
        thread.start()
        
    def run(self):
        self.download()
        self.segment()
        self.upload()

    def download(self):
        return download_blob(self.bucket_name,self.file_name,self.file_name)
        

    def segment(self):

        log = {
            self.file_name: "Em processamento"
        }
        f = open("/Shared/outfile.json", "r")
        old_file = f.read()
        f.close()
        outfile = open("/Shared/outfile.json",'w+')
        outfile.write(old_file + json.dumps(log))
        outfile.close()

        processo = "ffmpeg -ss 0 -i " + self.file_name + " -c copy -t "+ str(self.slice_time) +" "+ "/worker/corte_1_"+self.file_name
        subprocess.call(processo, shell=True)

        processo = "ffmpeg -ss " + str(self.slice_time) +" -i " + self.file_name + " -c copy "+ "/worker/corte_2_"+self.file_name
        subprocess.call(processo, shell= True )



    
    def upload(self):
        upload_blob(self.bucket_name,"/worker/" +"corte_1_"+self.file_name,"/corte_1_"+self.file_name)
        upload_blob(self.bucket_name,"/worker/" +"corte_2_"+self.file_name,"/corte_2_"+self.file_name)

        log = {
            self.file_name: [
                "corte_1_"+self.file_name, 
                "corte_2_"+self.file_name]
        }
        f = open("/Shared/outfile.json", "r")
        old_file = f.read()
        f.close()
        outfile = open("/Shared/outfile.json",'w+')
        outfile.write(old_file + json.dumps(log))
        outfile.close()

    






if __name__ == "__main__":  

    #processthread = threading.Thread(target = segment, args(1,))
    processo = []
    
    while(True):
        item = fila.get()
        print(item)
        processo.append(processThread(item['bucket_name'],item["file_name"],item["slice_time"]))

    
    #if mensagem:
    #    print("Mensagem recebida:")
    #    print(mensagem)

    
    #download_blob("tarefa-slicing-visio","1.mp4","/worker/1.mp4")

    print("Executou o worker!")